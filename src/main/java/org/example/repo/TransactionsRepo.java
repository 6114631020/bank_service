package org.example.repo;

import org.example.es.TransactionsEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TransactionsRepo extends ElasticsearchRepository<TransactionsEntity, String>{
    List<TransactionsEntity> findAll();
    List<TransactionsEntity> findByAccount(String account);
}
