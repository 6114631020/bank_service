package org.example.repo;

import org.example.es.AccountEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ServiceRepo extends ElasticsearchRepository<AccountEntity, String> {
    List<AccountEntity> findAll();
}