package org.example.controller;


import org.example.Bank;
import org.example.domain.Account;
import org.example.domain.Deposit;
import org.example.domain.Transfer;
import org.example.domain.Withdraw;
import org.example.es.AccountEntity;
import org.example.es.TransactionsEntity;
import org.example.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    private BankService service;


    @PutMapping("/createAccount")
    public Account createAccount(
            @RequestParam ("Name") String name , @RequestBody String idCard , @RequestParam("Bank") Bank bank){
        return service.createAccount(name,idCard,bank);
    }
    @PostMapping("/deposit")
    public Deposit deposit(
            @RequestParam("account") String account, @RequestParam("amount") double amount){
        return service.deposit(account, amount);
    }
    @PostMapping("/withdraw")
    public Withdraw withdraw(
            @RequestParam("account") String account, @RequestParam("amount") double amount){
        return service.withdraw(account, amount);
    }
    @PostMapping("/transfer")
    public Transfer transfer(
            @RequestParam("account") String account, @RequestParam("amount") double amount,String destinationAccount){
        return service.transfer(account, amount, destinationAccount);
    }

    @GetMapping("/getAllAccount")
    public List<AccountEntity> getAllAccount() { return service.getAllAccount(); }

    @GetMapping ("/transactionsByAccount")
    public List<TransactionsEntity> getTransactionsByAccount(String account){return service.getTransactionsByAccount(account);}

    @GetMapping("/checkTransactionDataById")
    public TransactionsEntity checkTransactionDataById(String id) {return service.checkTransactionDataById(id);}

    @GetMapping("/getByAccount")
    public  AccountEntity getByAccount(String account){return service.getByAccount(account);}

}
