package org.example.es;


import org.example.Bank;
import org.springframework.data.annotation.Id;

import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDateTime;

@Document(indexName = "bank-index")
public class AccountEntity {
    @Id
    private String account;
    @Field(type = FieldType.Keyword)
    private String name;
    private String idCard;
    private double money;
    private Bank bankName;
    @Field(type = FieldType.Date,format = DateFormat.basic_date_time)
    public LocalDateTime dateTime;

    public String getIdCard() {
        return idCard;
    }
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getMoney() {
        return money;
    }
    public void setMoneyD(double amount) {
        this.money = this.money + amount;
    }
    public void setMoneyW(double amount){this.money = this.money - amount;}
    public Bank getBankName() {
        return bankName;
    }
    public void setBankName(Bank bankName) {
        this.bankName = bankName;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
