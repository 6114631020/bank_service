package org.example.es;

import org.example.Bank;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDateTime;


@Document(indexName = "transactiond-index")
public class TransactionsEntity {
    @Id
    private String id;
    @Field(type = FieldType.Keyword)
    private String account;
    private String category;
    private String name;
    private Bank bankName;

    private String destinationName;
    private String destinationAccount;
    private Bank destinationBankName;

    private double amount;
    private double money;
    @Field(type = FieldType.Date,format = DateFormat.basic_date_time)
    private LocalDateTime dateTime;

    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDestinationName() {
        return destinationName;
    }
    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getDestinationAccount() {
        return destinationAccount;
    }
    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public double getMoney() {
        return money;
    }
    public void setMoney(double money) {
        this.money = money;
    }
    public Bank getBankName() {
        return bankName;
    }
    public void setBankName(Bank bankName) {
        this.bankName = bankName;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    public Bank getDestinationBankName() {
        return destinationBankName;
    }
    public void setDestinationBankName(Bank destinationBankName) {
        this.destinationBankName = destinationBankName;
    }
}
