package org.example.service;
import org.apache.commons.codec.language.bm.Rule;
import org.example.Bank;
import org.example.domain.Account;
import org.example.domain.Deposit;
import org.example.domain.Transfer;
import org.example.domain.Withdraw;
import org.example.es.AccountEntity;

import org.example.es.TransactionsEntity;
import org.example.repo.TransactionsRepo;
import org.example.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class BankService {
    @Autowired
    private ServiceRepo repo;
    @Autowired
    private TransactionsRepo tranRepo;


    public String createAccount(){return String.format("%05d",repo.count());}
    public String createCode(){return String.format("%05d",tranRepo.count());}
    public String randomCode(){
        Random randomCode = new Random();
        int code = randomCode.nextInt(50000+10000)-10000;
        return Integer.toString(code);
    }

    public Account createAccount(String name , String idCard, Bank bank){
        name = name.toLowerCase();
        Pattern formatIdCard = Pattern.compile("[0-9]{13}"); //Regex check
        Matcher checkIdCard = formatIdCard.matcher(idCard);
        Pattern formatName = Pattern.compile("[a-z]{1,20}"); //Regex check
        Matcher checkName = formatName.matcher(name);
        if(checkIdCard.matches()&& checkName.matches()){
            String code = randomCode();
            String account = code+createAccount();
            AccountEntity accountEntity = new AccountEntity();
            accountEntity.setName(name);
            accountEntity.setAccount(account);
            accountEntity.setBankName(bank);
            accountEntity.setIdCard(idCard);
            accountEntity.setDateTime(LocalDateTime.now());
            repo.save(accountEntity);
            Account account1 = new Account();
            account1.setName(name);
            account1.setAccount(account);
            account1.setBankName(bank);
            account1.setIdCard(idCard);
            account1.setDateTime(LocalDateTime.now());
            return account1;
        }else {
            if(!checkIdCard.matches()){throw new IllegalAccessError("Error :The ID card is invalid.");}
            if(!checkName.matches()){throw new IllegalAccessError("Error :Invalid name.");}
            else {throw new IllegalAccessError("Error :format is incorrect");}
            }
    }

    public AccountEntity getByAccount(String account){return repo.findById(account).get();}

    public Deposit deposit(String account, double amount){
            AccountEntity accountEntity = getByAccount(account);
            accountEntity.setMoneyD(amount);
            repo.save(accountEntity);
            TransactionsEntity transactionsEntity = new TransactionsEntity();
            String code = createCode();
            transactionsEntity.setId(code);
            transactionsEntity.setCategory("Deposit");
            transactionsEntity.setName(accountEntity.getName());
            transactionsEntity.setAmount(amount);
            transactionsEntity.setBankName(accountEntity.getBankName());
            transactionsEntity.setAccount(account);
            transactionsEntity.setDateTime(LocalDateTime.now());
            tranRepo.save(transactionsEntity);
            Deposit deposit = new Deposit();
            deposit.setId(code);
            deposit.setCategory("Deposit");
            deposit.setName(accountEntity.getName());
            deposit.setAmount(amount);
            deposit.setBankName(accountEntity.getBankName());
            deposit.setAccount(account);
            deposit.setDateTime(LocalDateTime.now());
            return deposit;
    }
    public Withdraw withdraw(String account, double amount){
        AccountEntity accountEntity = getByAccount(account);
        if(amount <= accountEntity.getMoney() && amount > 0){
            accountEntity.setMoneyW(amount);
            repo.save(accountEntity);
            TransactionsEntity transactionsEntity = new TransactionsEntity();
            String code = createCode();
            transactionsEntity.setId(code);
            transactionsEntity.setCategory("Withdraw");
            transactionsEntity.setName(accountEntity.getName());
            transactionsEntity.setAmount(amount);
            transactionsEntity.setBankName(accountEntity.getBankName());
            transactionsEntity.setAccount(account);
            transactionsEntity.setDateTime(LocalDateTime.now());
            tranRepo.save(transactionsEntity);
            Withdraw withdraw = new Withdraw();
            withdraw.setId(code);
            withdraw.setCategory("Withdraw");
            withdraw.setName(accountEntity.getName());
            withdraw.setAmount(amount);
            withdraw.setBankName(accountEntity.getBankName());
            withdraw.setAccount(account);
            withdraw.setDateTime(LocalDateTime.now());
            return withdraw;
        }
        else {
            throw new IllegalAccessError("format is incorrect");
        }
    }

    public Transfer transfer(String account, double amount, String destinationAccount){
        AccountEntity accountEntity = getByAccount(account);
        AccountEntity accountEntity1 = getByAccount(destinationAccount);
        TransactionsEntity transactionsEntity = new TransactionsEntity();
        if (amount <= accountEntity.getMoney() && amount > 0) {
            accountEntity.setMoneyW(amount);
            accountEntity1.setMoneyD(amount);
            repo.save(accountEntity);
            repo.save(accountEntity1);
            String code = createCode();
            transactionsEntity.setId(code);
            transactionsEntity.setCategory("Transfer");
            transactionsEntity.setAccount(account);
            transactionsEntity.setName(accountEntity.getName());
            transactionsEntity.setBankName(accountEntity.getBankName());
            transactionsEntity.setAmount(amount);
            transactionsEntity.setDestinationAccount(destinationAccount);
            transactionsEntity.setDestinationName(accountEntity1.getName());
            transactionsEntity.setDestinationBankName(accountEntity1.getBankName());
            transactionsEntity.setDateTime(LocalDateTime.now());
            tranRepo.save(transactionsEntity);
            Transfer transfer = new Transfer();
            transfer.setId(code);
            transfer.setCategory("Transfer");
            transfer.setAccount(account);
            transfer.setName(accountEntity.getName());
            transfer.setBankName(accountEntity.getBankName());
            transfer.setAmount(amount);
            transfer.setDestinationAccount(destinationAccount);
            transfer.setDestinationName(accountEntity1.getName());
            transfer.setDestinationBankName(accountEntity1.getBankName());
            transfer.setDateTime(LocalDateTime.now());
            return transfer;
        }
        else {
            throw new IllegalAccessError("format is incorrect");
        }
    }
    public List<AccountEntity> getAllAccount(){return repo.findAll();}

    public List<TransactionsEntity> getTransactionsByAccount(String account) {
        return tranRepo.findByAccount(account);
    }

    public  TransactionsEntity findById(String id){return tranRepo.findById(id).get();}


    public TransactionsEntity checkTransactionDataById(String id){
        TransactionsEntity transactionsEntity = findById(id);
        if(Integer.parseInt(id) == Integer.parseInt(transactionsEntity.getId())){
            return tranRepo.findById(id).get();}
        else {throw new IllegalAccessError("No data found.");}
        }

}
